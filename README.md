# SkunkItemTransfer  

A Asheron's Call [SkunkWorks](http://skunkworks.sourceforge.net) library

## Functionality
- Combine OnChatServer and OnAddToInventory/OnRemoveFromInventory into one event containing the item and receiver/giver.

## Example
In your swx file.
`<script src="modules\\SkunkItemTransfer\\SkunkItemTransfer.js"/>`
In your js file.
```
var SkunkItemTransfer     = LibStub && LibStub("SkunkItemTransfer-1.0") || SkunkItemTransfer10;
SkunkItemTransfer.addCallback(onSkunkItemTransfer);
function onSkunkItemTransfer( params ) {
	var aco = params.aco; // The item object.
	var szWho = params.szWho // The sender's name.
	var evid = params.evid; // The Skunkworks event Id. evidOnAddToInventory or evidOnRemoveFromInventory
	var quantity = params.quantity; // The amount of item we received, based on chat chat message.
	if (evid == evidOnAddToInventory) {
		skapi.OutputSz(szWho + " gave us " + SkunkItemTransfer.getFullName(aco) + " * " + quantity, opmConsole);
	}
}
```


## External links
- [SkunkItemTransfer source](https://gitlab.com/Cyprias/SkunkItemTransfer)  
- [SkunkWorks Discord channel](https://discord.gg/z5wXR5K)  

## Contributors
- [Cyprias](https://gitlab.com/cyprias)

## License
MIT License	(http://opensource.org/licenses/MIT)