/********************************************************************************************\
	File Name:      SkunkItemTransfer.js
	Purpose:        Combine OnChatServer and OnAddToInventory/OnRemoveFromInventory into one event containing the item and receiver/giver.
	Creator:        Cyprias
	Date:           09/24/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "SkunkItemTransfer-1.0";
var MINOR = 180417;

(function (factory) {
	// Check if script was loaded via require().
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
        SkunkItemTransfer10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	core.debugging = false;
	
	/******************************************************************************\
		fConsole: Print a message to console.
	\******************************************************************************/
	function fConsole(szMsg) {
		skapi.OutputSz(szMsg + "\n", opmConsole);
	};
		
	/******************************************************************************\
		debug: Print a message to console if debugging is enabled.
	\******************************************************************************/
	core.debug = function debug(szMsg) {
		if (core.debugging == true) {
			fConsole("[SkunkItemTransfer.js] "+szMsg);
		}
	};


	var _callbacks = [];
	/******************************************************************************\
		addCallback: Add a evid callback.
	\******************************************************************************/
	core.addCallback = function addCallback(method, thisArg) {
		_callbacks.push({method:method, thisArg:thisArg});
	};
	
	/******************************************************************************\
		removeCallback: Remove a callback function.
	\******************************************************************************/
	core.removeCallback = function removeCallback(method, thisArg) {
		for (var i=_callbacks.length-1; i>=0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i,1);
			}
		}
	};
	
	/******************************************************************************\
		fFireCallback: Fire a registered callback.
	\******************************************************************************/
	function fFireCallback() {
		core.debug(_callbacks.length + " callbacks to fire.");

		for ( var i=_callbacks.length-1; i >= 0; i-- ) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	};
	
	function SplitString(sz, seperator) {
		if (typeof seperator === "undefined") seperator = " ";
		var parts = sz.split(seperator);
		var strings = [];
		var part;
		var longString = false;
		for ( var i=0; i < parts.length; i++ ) {
			part = parts[i];
			
			if (part.substr(0,1) == "'") {
				strings.push(part.substr(1));
				longString = true;
				continue;
			} else if (part.substr(part.length-1) == "'") {
				strings[strings.length-1] += seperator + part.substr(0, part.length-1);
				longString = false;
				continue;
			}
			if (longString == true) {
				strings[strings.length-1] += seperator + part;
			} else {
				strings.push(part);
			}
		}
		
		return strings;
	}

	
	core.escapeRegExp = function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	};
	
	core.getAcoFromPluralName = function getAcoFromPluralName(szName) {
		var acf = skapi.AcfNew();	
		var coaco = acf.CoacoGet();
		for (var i = 0; i < coaco.Count; i++) {
			aco = coaco.Item(i);
			if (aco.szPlural.match(szName)) {
				return aco;
			}
		}
	};
	
	/*
		When receving items, OnChatServer fires then OnAddToInventory.
		When giving items, OnRemoveFromInventory fires then OnChatServer.
	*/

	var itemsReceived   = {};

	var handler = {};
	handler.OnChatServer = function OnChatServer(szMsg, cmc) {
		core.debug("<OnChatServer> " + szMsg);

		if (szMsg.match(/^(.*) gives you ([0-9,]*) (.*).$/i)) {
			var szWho = RegExp.$1;
			var quantity = RegExp.$2;
			var szItem = RegExp.$3;
			
			quantity = Number( quantity.replace(/,/g, '') ); // remove comma and convert to number.

			core.debug(" szWho: " + szWho + ", quantity: " + quantity + ", szItem: " + szItem);

			var acoItem = skapi.AcoFromSz(szItem) || getAcoFromPluralName( escapeRegExp(szItem) );
			
			core.debug(" acoItem: " + acoItem);
			
			szItem = acoItem && acoItem.szName || szItem;
			core.debug(" szItem2: " + szItem);
			
			itemsReceived[szItem] = {};
			itemsReceived[szItem].quantity  = quantity;
			itemsReceived[szItem].szWho     = szWho;
			itemsReceived[szItem].time      = new Date().getTime();
		} else if(szMsg.match(/^(.*) gives you (.*).$/i)) {
			var szWho = RegExp.$1;
			var szItem = RegExp.$2;
			core.debug("szWho: " + szWho + ", szItem: " + szItem);
			
			itemsReceived[szItem] = {};
			itemsReceived[szItem].quantity  = 1;
			itemsReceived[szItem].szWho     = szWho;
			itemsReceived[szItem].time      = new Date().getTime();
		} else if(szMsg.match(/^You give (.*) (\d*) (.*).$/i)) {
			var szWho = RegExp.$1;
			var quantity = Number(RegExp.$2);
			var szItem = RegExp.$3;

			var acoItem = skapi.AcoFromSz(szItem) || getAcoFromPluralName(escapeRegExp(szItem));
			szItem = acoItem && acoItem.szName || szItem;
			
			if (removedItems[szItem]) {
				fFireCallback({
					evid        : evidOnRemoveFromInventory,
					aco         : removedItems[szItem].aco,
					szWho       : szWho,
					quantity    : quantity
				});
				delete removedItems[szItem];
			}
		} else if(szMsg.match(/^You give (.*).$/i)) {
		
			var szWhoAndItem = RegExp.$1;
			// Since there's nothing seperating the person and item names, we need to split the string and search for the aco of the receiver.
			
			var parts = SplitString(szWhoAndItem);
			var szWho = parts[0];
			var acoWho = skapi.AcoFromSz(szWho);
			var itemIndex = 1;
			if (!acoWho) {
				for ( var i=1; i < parts.length; i++ ) {
					itemIndex = i+1;
					szWho += " " + parts[i];
					acoWho = skapi.AcoFromSz(szWho);
					if (acoWho) break;
				}
			}
			core.debug("acoWho: " + acoWho + " (" + szWho + ")");
			if (!acoWho) return;
			
			var szItem = parts[itemIndex];
			for ( var i=itemIndex+1; i < parts.length; i++ ) {
				szItem += " " + parts[i];
			}
			core.debug("szItem: " + szItem);

			var acoItem = skapi.AcoFromSz(szItem) || getAcoFromPluralName(escapeRegExp(szItem));
			szItem = acoItem && acoItem.szName || szItem;
			
			if (removedItems[szItem]) {
				fFireCallback({
					evid        : evidOnRemoveFromInventory,
					aco         : removedItems[szItem].aco,
					szWho       : szWho,
					quantity    : 1
				});
				delete removedItems[szItem];
			}
		} else {
			core.debug("<OnChatServer> UNCAUGHT: " + szMsg);
		}
	};
	
	handler.OnAddToInventory = function OnAddToInventory(aco) {
		core.debug("<OnAddToInventory> " + aco);
		var szName = core.getFullName(aco);
		
		if (itemsReceived[szName]) {
			core.debug("We received " + szName + " from " + itemsReceived[szName].szWho);
			fFireCallback({
				evid        : evidOnAddToInventory,
				aco         : aco,
				szWho       : itemsReceived[szName].szWho,
				quantity    : itemsReceived[szName].quantity
			});
			delete itemsReceived[szName];
		} else {
			// Check for aco.szPlural
			
			core.debug(" szPlural: " + aco.szPlural);
			 
			for (var szName in itemsReceived)  {
				if (!itemsReceived.hasOwnProperty(szName)) continue;
				core.debug(" szName: " + szName);
				if (aco.szPlural.match( escapeRegExp(szName) )) {
					
					core.debug("We may have received " + szName + " from " + itemsReceived[szName].szWho);
					fFireCallback({
						evid        : evidOnAddToInventory,
						aco         : aco,
						szWho       : itemsReceived[szName].szWho,
						quantity    : itemsReceived[szName].quantity
					});
					delete itemsReceived[szName];
					
					return;
				}
			}
			
			
			core.debug(szName + " from unknown!");
		}
	};

	var removedItems = {};
	handler.OnRemoveFromInventory = function OnRemoveFromInventory(aco, acoContainer, iitem) {
		var szName = core.getFullName(aco);
		core.debug("<OnRemoveFromInventory> " + aco + ", " + acoContainer + ", " + iitem);
		removedItems[szName] = {}//new Date();
		removedItems[szName].aco    = aco;
		removedItems[szName].time   = new Date().getTime();
	};
	
	skapi.AddHandler(evidOnChatServer,              handler);
	skapi.AddHandler(evidOnAddToInventory,          handler);
	skapi.AddHandler(evidOnRemoveFromInventory,     handler);
	
	// Return a aco's name with its material name.
	core.getFullName = function factory() {
		var materials = {};
		materials[materialAgate]            = "Agate";
		materials[materialAlabaster]        = "Alabaster";
		materials[materialAmber]            = "Amber";
		materials[materialAmethyst]         = "Amethyst";
		materials[materialAquamarine]       = "Aquamarine";
		materials[materialAzurite]          = "Azurite";
		materials[materialBlackGarnet]      = "Black Garnet";
		materials[materialBlackOpal]        = "Black Opal";
		materials[materialBloodstone]       = "Bloodstone";
		materials[materialBrass]            = "Brass";
		materials[materialBronze]           = "Bronze";
		materials[materialCarnelian]        = "Carnelian";
		materials[materialCeramic]          = "Ceramic";
		materials[materialCitrine]          = "Citrine";
		materials[materialCloth]            = "Cloth";
		materials[materialCopper]           = "Copper";
		materials[materialDiamond]          = "Diamond";
		materials[materialDilloHide]        = "Dillo Hide";
		materials[materialEbony]            = "Ebony";
		materials[materialEmerald]          = "Emerald";
		materials[materialFireOpal]         = "Fire Opal";
		materials[materialGem]              = "Gem";
		materials[materialGold]             = "Gold";
		materials[materialGranite]          = "Granite";
		materials[materialGreenGarnet]      = "Green Garnet";
		materials[materialGreenJade]        = "Green Jade";
		materials[materialGromnieHide]      = "Gromnie Hide";
		materials[materialHematite]         = "Hematite";
		materials[materialImperialTopaz]    = "Imperial Topaz";
		materials[materialIron]             = "Iron";
		materials[materialIvory]            = "Ivory";
		materials[materialJet]              = "Jet";
		materials[materialLapisLazuli]      = "Lapis Lazuli";
		materials[materialLavenderJade]     = "Lavender Jade";
		materials[materialLeather]          = "Leather";
		materials[materialLinen]            = "Linen";
		materials[materialMahogany]         = "Mahogany";
		materials[materialMalachite]        = "Malachite";
		materials[materialMarble]           = "Marble";
		materials[materialMetal]            = "Metal";
		materials[materialMoonstone]        = "Moonstone";
		materials[materialOak]              = "Oak";
		materials[materialObsidian]         = "Obsidian";
		materials[materialOnyx]             = "Onyx";
		materials[materialOpal]             = "Opal";
		materials[materialPeridot]          = "Peridot";
		materials[materialPine]             = "Pine";
		materials[materialPorcelain]        = "Porcelain";
		materials[materialPyreal]           = "Pyreal";
		materials[materialRedGarnet]        = "Red Garnet";
		materials[materialRedJade]          = "Red Jade";
		materials[materialReedsharkHide]    = "Reedshark Hide";
		materials[materialRoseQuartz]       = "Rose Quartz";
		materials[materialRuby]             = "Ruby";
		materials[materialSandstone]        = "Sandstone";
		materials[materialSapphire]         = "Sapphire";
		materials[materialSatin]            = "Satin";
		materials[materialSerpentine]       = "Serpentine";
		materials[materialSilk]             = "Silk";
		materials[materialSilver]           = "Silver";
		materials[materialSmokeyQuartz]     = "Smokey Quartz";
		materials[materialSteel]            = "Steel";
		materials[materialStone]            = "Stone";
		materials[materialSunstone]         = "Sunstone";
		materials[materialTeak]             = "Teak";
		materials[materialTigerEye]         = "Tiger Eye";
		materials[materialTourmaline]       = "Tourmaline";
		materials[materialTurquoise]        = "Turquoise";
		materials[materialVelvet]           = "Velvet";
		materials[materialWhiteJade]        = "White Jade";
		materials[materialWhiteQuartz]      = "White Quartz";
		materials[materialWhiteSapphire]    = "White Sapphire";
		materials[materialWood]             = "Wood";
		materials[materialWool]             = "Wool";
		materials[materialYellowGarnet]     = "Yellow Garnet";
		materials[materialYellowTopaz]      = "Yellow Topaz";
		materials[materialZircon]           = "Zircon";

		return function getFullName( aco ) {
			var szName = aco.szName;
			if (aco && aco.material > 0 && materials[aco.material]) {
				szName = materials[aco.material] + " " + aco.szName;
			}
			return szName;
		};
	}();
	
	
	// Public API
	return core;
}));